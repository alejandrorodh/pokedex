import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
// import { createStackNavigator, createAppContainer } from "react-navigation";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import PokedexHomeScreen from './app/screens/pokedexHome';
import PokemonDetailsScreen from './app/screens/pokemonDetails';


const AppNavigator = createStackNavigator();

function Stack() {
  return (
    <AppNavigator.Navigator>
      <AppNavigator.Screen name="Pokedex" component={PokedexHomeScreen} />
      <AppNavigator.Screen name="Pokemon" component={PokemonDetailsScreen} />
    </AppNavigator.Navigator>
  );
}

export default class App extends React.Component {
  render() {
    return (
      <NavigationContainer>
        <Stack />
      </NavigationContainer>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F0F2F4',
    alignItems: 'center',
    justifyContent: 'center',
  },
});