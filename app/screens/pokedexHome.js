import React, { Component } from 'react';
import { Button, View, Text, Image } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import Card from '../components/card/card.component';

export default class PokedexHomeScreen extends Component {
	constructor(props) {
		super(props);

		this.state = {
			pokemon: [],
			currentOffset: 0
		};
	}

	fetchPokemonIndex = () => {
		fetch('https://pokeapi.co/api/v2/pokemon/?offset=' + this.state.currentOffset + '&limit=30')
			.then(response => response.json())
			.then((responseJson) => {
				this.setState((prevState) => ({
					pokemon: prevState.currentOffset === 0 ? responseJson.results : [...prevState.pokemon, ...responseJson.results],
					currentOffset: prevState.currentOffset + 30
				}))
			})
			.catch(error => console.log('Fetching error: ' + error));
	}

	componentDidMount() {
		this.fetchPokemonIndex();
	}

	render() {
		return (
			<View style={{ backgroundColor: '#F0F2F4', flex: 1, justifyContent: 'center' }}>
				<FlatList
					contentContainerStyle={{ padding: 15, flexGrow: 1 }}
					data={this.state.pokemon}
					numColumns={2}
					keyExtractor={(item, index) => item.name.toString()}
					ItemSeparatorComponent={() => <View style={{ padding: 10, }} />}
					renderItem={({ item, index }) => (
						<Card touchable style={{ flex: 1, alignItems: 'center', marginHorizontal: 10, padding: 30, }} pokemonData={item}>
							<Image style={{ width: 100, height: 100 }} source={{
								uri: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${item.url.slice(0, -1).replace('https://pokeapi.co/api/v2/pokemon/', '')}.png`
							}} />
							<Text style={{ fontWeight: 'bold' }}>{item.name}</Text>
						</Card>
					)}
					onEndReachedThreshold={0}
					onEndReached={this.fetchPokemonIndex}
				/>
			</View>
		)
	}
}