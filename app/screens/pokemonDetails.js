import React, { Component } from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';
import Card from '../components/card/card.component';
import PokemonTypesStyles from '../styles/pokemonTypes.styles';

export default class PokemonDetailsScreen extends Component {
	constructor(props) {
		super(props);

		this.state = {
			pokemon: {},
			pokemonType: []
		};
	}

	fetchPokemonData = () => {
		const { pokemonData } = this.props.route.params;
		fetch(pokemonData.url)
			.then(response => response.json())
			.then((responseJson) => {
				this.setState({
					pokemon: responseJson,
					pokemonImage: responseJson.sprites.front_default,
					pokemonType: responseJson.types
				})
			})
			.catch(error => console.log('Fetching error: ' + error));
	}

	setPokemonTypeStyle = (type) => {
		switch (type) {
			case 'normal':
				return PokemonTypesStyles.normal;
			case 'fighting':
				return PokemonTypesStyles.fighting;
			case 'flying':
				return PokemonTypesStyles.flying;
			case 'poison':
				return PokemonTypesStyles.poison;
			case 'ground':
				return PokemonTypesStyles.ground;
			case 'rock':
				return PokemonTypesStyles.rock;
			case 'bug':
				return PokemonTypesStyles.bug;
			case 'ghost':
				return PokemonTypesStyles.ghost;
			case 'steel':
				return PokemonTypesStyles.steel;
			case 'fire':
				return PokemonTypesStyles.fire;
			case 'water':
				return PokemonTypesStyles.water;
			case 'grass':
				return PokemonTypesStyles.grass;
			case 'electric':
				return PokemonTypesStyles.electric;
			case 'psychic':
				return PokemonTypesStyles.psychic;
			case 'ice':
				return PokemonTypesStyles.ice;
			case 'dragon':
				return PokemonTypesStyles.dragon;
			case 'fairy':
				return PokemonTypesStyles.fairy;
			case 'dark':
				return PokemonTypesStyles.dark;
			default:
				return PokemonTypesStyles.normal;
		}
	}

	componentDidMount() {
		this.fetchPokemonData();
	}

	render() {
		return (
			<View style={styles.mainContainer}>
				<Image style={{ width: 300, height: 300, alignSelf: 'center', }} source={{ uri: this.state.pokemonImage }} />
				<Card>
					<View style={[styles.row, { justifyContent: 'space-between', paddingBottom: 30, }]}>
						<Text style={styles.title}>
							{this.state.pokemon.name}
							<Text style={styles.subtitle}>{' #' + this.state.pokemon.id}</Text>
						</Text>
						<View style={{ flexDirection: 'row' }}>
							{
								this.state.pokemonType.map((t) => {
									let typeClass = '';
									return (
										<View style={[styles.type, this.setPokemonTypeStyle(t.type.name)]}>
											<Text style={styles.typeText}>{t.type.name}</Text>
										</View>
									)
								})
							}
						</View>
					</View>
					<View style={styles.row}>
						<Text style={styles.label}>Height</Text>
						<Text style={styles.data}>{this.state.pokemon.height + 'dm'}</Text>
					</View>
					<View style={styles.row}>
						<Text style={styles.label}>Weight</Text>
						<Text style={styles.data}>{this.state.pokemon.weight + 'hg'}</Text>
					</View>
					<View style={styles.row}>
						<Text style={styles.label}>Category</Text>
						<Text style={styles.data}>{this.state.pokemon.height}</Text>
					</View>
					<View style={styles.row}>
						<Text style={styles.label}>Ability</Text>
						<Text style={styles.data}>{this.state.pokemon.weight}</Text>
					</View>
				</Card>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	mainContainer: {
		padding: 15,
		backgroundColor: '#F0F2F4',
		flex: 1,
	},
	row: {
		flexDirection: 'row'
	},
	title: {
		fontSize: 24,
		fontWeight: 'bold',
		color: '#707070'
	},
	subtitle: {
		fontSize: 22,
		fontWeight: 'bold',
		color: '#ADADAD'
	},
	label: {
		width: '40%',
		fontSize: 16,
		fontWeight: 'bold',
		color: '#ADADAD'
	},
	data: {
		flexGrow: 1,
		fontSize: 16,
		fontWeight: 'bold',
		color: '#707070'
	},
	type: {
		padding: 5,
		marginHorizontal: 5,
		borderRadius: 3,
	},
	typeText: {
		fontWeight: 'bold',
		color: '#ffffff'
	},
});