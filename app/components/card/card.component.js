import React, { Component } from 'react';
import { StyleSheet, View, Pressable } from 'react-native';
import { useNavigation } from '@react-navigation/native';

class CardComponent extends Component {
    constructor(props) {
        super(props);
    }

    GoToPokemon = () => {
        const { navigation } = this.props;
        navigation.navigate('Pokemon', {
            pokemonData: this.props.pokemonData
        })
    }

    render() {
        return (
            this.props.touchable
                ?
                <Pressable onPress={this.GoToPokemon}
                    style={({ pressed }) => [
                        {
                            backgroundColor: pressed
                                ? '#f7f7f7'
                                : '#fff'
                        },
                        styles.wrapper,
                        this.props.style
                    ]}>
                    {this.props.children}
                </Pressable>
                :
                <View style={[styles.wrapper, this.props.style, { backgroundColor: '#ffffff' }]}>
                    {this.props.children}
                </View>
        )
    }
}

const styles = StyleSheet.create({
    wrapper: {
        padding: 20,
        borderRadius: 6,

        shadowColor: "#242624",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.15,
        shadowRadius: 2,

        elevation: 2,
    }
});

export default function (props) {
    const navigation = useNavigation();
    return <CardComponent {...props} navigation={navigation} />;
}