import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    normal: {
        backgroundColor: 'grey'
    },
    fighting: {
        backgroundColor: 'brown'
    },
    flying: {
        backgroundColor: 'skyblue'
    },
    poison: {
        backgroundColor: 'mediumpurple'
    },
    ground: {
        backgroundColor: 'darkgoldenrod'
    },
    rock: {
        backgroundColor: 'dimgrey'
    },
    bug: {
        backgroundColor: 'forestgreen'
    },
    ghost: {
        backgroundColor: 'slateblue'
    },
    steel: {
        backgroundColor: 'lightsteelblue'
    },
    fire: {
        backgroundColor: 'darkorange'
    },
    water: {
        backgroundColor: 'steelblue'
    },
    grass: {
        backgroundColor: 'green'
    },
    electric: {
        backgroundColor: 'gold'
    },
    psychic: {
        backgroundColor: 'deeppink'
    },
    ice: {
        backgroundColor: 'darkturquoise'
    },
    dragon: {
        backgroundColor: 'crimson'
    },
    fairy: {
        backgroundColor: 'hotpink'
    },
    dark: {
        backgroundColor: 'purple'
    },
})